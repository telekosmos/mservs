'use strict';

const fs = require('fs');

// To use the init function parametrized, the params has to be in options object
module.exports = function (options) {
	var logger;

	const init = function (msg, reply) {
		// setup loggin into a file
		console.log(`Initiating logfile '${options.logfile}'`);
		fs.open(options.logfile, 'a', (err, fd) => {
			if (err)
				console.error(err);

			logger = makeLogger(fd); // returns a function to insert rows
			reply();
		})
	};

	const makeLogger = function (fd) {
		return function (entry) {
			fs.write(fd, `[${new Date().toISOString()}] ${entry}\n`, null, 'utf8', function (err) {
				if (err)
					console.error(err);
				fs.fsync(fd, (err) => {
					if (err)
						console.error(`ERR flushing log file: ${JSON.stringify(err)}`);
				})
			})
		}
	}

	// SERVICES
	this.add('misc:ping', (msg, resp) => {
		logger(`Pinging`);
		resp(null, {
			reply: 'PING: ' + new Date().toDateString()
		})
	});

	this.add('misc:hello', (msg, resp) => resp(null, {
		reply: 'Hello ' + msg.name
	}));

	this.add('init:misc', init);

}