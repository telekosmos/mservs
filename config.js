'use strict';

module.exports = {
    host: 'localhost',
    port: '6969',
    httpCfg: {type: 'http', host: this.host, port: this.port},
    tcpCfg: {type: 'tcp'}
};
