'use strict'

var Seneca = require('seneca');
var Express = require('express');
var Web = require('seneca-web');
var ApiPlugin = require('./apiplugin');
var MathPlugin = require('./math');

var Routes = [
  {
    prefix: '/todo',
    pin: 'role:todo,cmd:*',
    map: {
      list: true,
      edit: {
        GET: true
      }
    }
  },
  {
    prefix: '/admin',
    pin: 'role:admin,cmd:*',
    map: {
      validate: {
        // POST: true,
        GET: true
        // alias: '/manage' // invalidates /admin/validate 
      },
      create: {GET: true}
    }
  }, {
    prefix: '/api',
    pin: 'role:api,cmd:*',
    map: {
      misc: {GET:true, suffix: '/:what'},
      calculate: { GET:true, suffix:'/:operation' }
    }
  }
];

var config = {
  routes: Routes,
  adapter: require('seneca-web-adapter-express'),
  context: Express()
};

var seneca = Seneca()
  .use(ApiPlugin)
  .use(MathPlugin) // Has to use another service in order to be able to use it in here
  .use(Web, config)
  .ready(() => {
    var server = seneca.export('web/context')();

    server.listen('4000', () => {
      console.log('server started on: 4000')
    })
});