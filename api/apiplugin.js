'use strict'

module.exports = function plugin() {
  var seneca = this

  seneca.add('role:todo,cmd:list', (msg, done) => {
    done(null, {
      ok: true
    })
  })

  seneca.add('role:todo,cmd:edit', (msg, done) => {
    var rep = msg.response$
    // console.log(`${JSON.stringify(rep)}`);

    // Custom handlers send back request and response
    // Objects who may not have the same shape. Bear
    // in mind accessing these objects limits the
    // ability to swap frameworks easily.
    // Not working
    /*
    if (rep.send) {
      rep.send(msg.args)
    } else {
      rep(null, msg.args)
    }
    */
    console.log('@@@@@@@@@@@ ROLE:TODO,CMD:EDIT!!!');
    done({msg: 'role:todo,cmd:edit'});
  })

  seneca.add('role:admin,cmd:create', (msg, done) => {
    done(null, {ok: true, args: msg.args, cmd: 'create'});
  });

  seneca.add('role:admin,cmd:validate', (msg, done) => {
  // seneca.add('role:admin,cmd:manage', (msg, done) => {
    console.log('role:admin,cmd:manage');
    done(null, {
      ok: true,
      args: msg.args,
      cmd: 'manage!!'
    })
  })

  
  var valid_ops = {
    sum: 'sum',
    product: 'product'
  }

  this.add('role:api,cmd:calculate', function (msg, respond) {
    console.log('role:api,cmd:calculate')
    var operation = msg.args.params.operation
    var left = msg.args.query.left
    var right = msg.args.query.right
    this.act('role:math', {
      cmd: valid_ops[operation],
      left: left,
      right: right,
    }, respond)
  });


  this.add('role:api,cmd:misc', function (msg, respond) {
    // this.act('misc:ping', respond);
    // console.log('************** About to call service misc:hello ******************');
    // this.act('misc:hello', {name: 'Jack'}, respond);
    console.log('*** role:api,cmd:misc -> what: '+msg.args.params.what);
    this.act('role:math', {
      cmd: valid_ops['product'],
      left: 2,
      right: 13,
    }, respond);
  });
  
}