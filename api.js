'use strict';

module.exports = function api(options) {

  var valid_ops = { sum:'sum', product:'product' }

  this.add('role:api,crap:calculate', function (msg, respond) {
    var operation = msg.args.params.operation
    var left = msg.args.query.left
    var right = msg.args.query.right
    this.act('role:math', {
      cmd:   valid_ops[operation],
      left:  left,
      right: right,
    }, respond)
  });


  this.add('role:api,path:ping', function (msg, respond) {
    // this.act('misc:ping', respond);
    console.log('************** About to call service misc:hello ******************');
    // this.act('misc:hello', {name: 'Jack'}, respond);
    this.act('role:math', {
      cmd:   valid_ops['product'],
      left:  2,
      right: 13,
    }, respond);
  });

  this.add('init:api', function (msg, respond) {
    this.act('role:web',{routes: [{
      prefix: '/api',
      pin:    'role:api,crap:*',
      map: {
        calculate: { GET:true, suffix:'/:operation' }
      }
    }, {
      prefix: '/api',
      pin:    'role:api,path:ping',
      map: {
        ping: {GET: true}
      }
    }]}, respond)
  })

}