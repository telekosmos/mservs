'use strict';

let seneca = require('seneca')();

seneca.use('math'); // finds ./math.js in local folder
seneca.use('misc', {logfile: 'mservs.log'});

module.exports = seneca;
