'use strict';

const config = require('./config');
// let seneca = require('./servs');
let seneca = require('seneca')();
let client = seneca.client(config.httpCfg);
// module.exports = client;

client
.act('misc:ping', (err, val) => {
    const msg = val.reply;
    console.log(`MServer responded: ${msg}`);
})
.act('role:math', {cmd:'product',left:8, right:3}, (err, val) => console.log(`Result (from object) is ${val.answer}`))
.act('misc:hello', {name: 'Jack'}, (err, val) => console.log(`Resp: ${val.reply}`));