'use strict';

let client = require('./client');

client
.act('misc:ping', (err, val) => {
    const msg = val.reply;
    console.log(`MServer responded: ${msg}`);
})
.act('role:math,cmd:product,left:8, right:3', (err, val) => console.log(`Result is ${val.answer}`));