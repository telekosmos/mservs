
# Just a playground repo for Seneca

In very early stage.

So far, a skeleton for Seneca microservices with a kind of api-gateway plugin ([seneca-web](https://github.com/senecajs/seneca-web)) to map http requests to actions. Right now mostly a bit of mishmash.

### Get
`git clone https://telekosmos@bitbucket.org/telekosmos/mservs.git && cd mservs`


### Use
The `api` folder is addressed to build an api gateway and services.

`npm install`

`node api/server.js # startup api gateway and services`

From another terminal:

```
$ curl 'http://localhost:4000/api/calculate/product?left=7&right=12
{"answer": 84}

$ curl 'http://localhost:4000/admin/create'
{"ok":true,"args":{"body":"","route":{"prefix":"/admin","postfix":false,"suffix":false,"part":"create","pin":"role:admin,cmd:*","alias":false,"methods":["GET"],"autoreply":true,"redirect":false,"auth":false,"middleware":false,"secure":false,"pattern":"role:admin,cmd:create","path":"/admin/create"},"params":{},"query":{},"user":null},"cmd":"create"}
```

On the other hand, there is a minimal implementation of a programmatic client. To try it, assuming previous `npm install`:
```
node server.js

# on another terminal, same folder
node client.js
```
